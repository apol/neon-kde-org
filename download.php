<?php
/**
 * Available variables for the header:
 * $headerCssFiles = [];
 * $headerJsFiles = [];
 * $headerBodyClasses = '';
 */
    $headerCssFiles = ['content/download.css'];
    $headerBodyClasses = '';
    include('templates/header.php');
    include('lib/IsoDates.php');
    $isoDates = new IsoDates();
    $isoDates->init();
?>
<main class="KLayout">
    <section class="container" id="KNeonArticle">
        <div class="row text-center">
            <article>
                <h1 id="live">Live Images</h1>
                <p>
                    Live images are ideal for people who want a clean installation. Use a live image to replace your existing system, install alongside existing operating systems, or simply try KDE neon without affecting their computer.
                </p>
            </article>

            <div class="col-12 col-md-6 vertical-border" >
                <img src="content/download/userMedia.svg" class="splashImage" />
                <h3>User Edition<sup class="tag">64-bit</sup></h3>
                <p class="text-center">
                    Featuring the latest officially released KDE software on a stable base. Ideal for everyday users.
                </p>
                <div class="downloadButton">
                    <a href="https://files.kde.org/neon/images/user/<?php print $isoDates->user ?>/neon-user-<?php print $isoDates->user ?>.iso">User Edition Live/Install Image</a>
                </div>
                <br />
                <a href="https://files.kde.org/neon/images/user/<?php print $isoDates->user ?>/neon-user-<?php print $isoDates->user ?>.iso.sig">PGP signature for verification</a>
                <br />
                <a href="https://files.kde.org/neon/images/user/<?php print $isoDates->user ?>/neon-user-<?php print $isoDates->user ?>.iso.torrent">Torrent file</a>
            </div>

            <div class="col-12 col-md-6 vertical-border" >
                <img src="content/download/developerMedia.svg" class="splashImage" />
                <h3>Testing Edition<sup class="tag">64-bit</sup></h3>
                <p class="text-center">
                    Featuring pre-release KDE software built the same day from bugfix branches. Good for testing. There is no QA. Will contain bugs.
                </p>
                <div class="downloadButton">
                    <a href="https://files.kde.org/neon/images/testing/<?php print $isoDates->testing ?>/neon-testing-<?php print $isoDates->testing ?>.iso">Testing Edition Live/Install Image</a>
                </div>
                <br />
                <a href="https://files.kde.org/neon/images/testing/<?php print $isoDates->testing ?>/neon-testing-<?php print $isoDates->testing ?>.iso.sig">PGP signature for verification</a>
                <br />
                <a href="https://files.kde.org/neon/images/testing/<?php print $isoDates->testing ?>/neon-testing-<?php print $isoDates->testing ?>.iso.torrent">Torrent file</a>

                <h3>Unstable Edition<sup class="tag">64-bit</sup></h3>
                <p class="text-center">
                    Featuring pre-release KDE software built the same day from new feature branches. Good for testing. There is no QA. Will contain many bugs.
                </p>
                <div class="downloadButton">
                    <a href="https://files.kde.org/neon/images/unstable/<?php print $isoDates->unstable ?>/neon-unstable-<?php print $isoDates->unstable ?>.iso">Unstable Edition Live/Install Image</a>
                </div>
                <br />
                <a href="https://files.kde.org/neon/images/unstable/<?php print $isoDates->unstable ?>/neon-unstable-<?php print $isoDates->unstable ?>.iso.sig">PGP signature for verification</a>
                <br />
                <a href="https://files.kde.org/neon/images/unstable/<?php print $isoDates->unstable ?>/neon-unstable-<?php print $isoDates->unstable ?>.iso.torrent">Torrent file</a>

                <h3>Developer Edition<sup class="tag">64-bit</sup></h3>
                <p class="text-center">
                    Unstable Editon plus development libraries pre-installed.
                </p>
                <div class="downloadButton">
                    <a href="https://files.kde.org/neon/images/developer/<?php print $isoDates->developer ?>/neon-developer-<?php print $isoDates->developer ?>.iso">Developer Edition Live/Install Image</a>
                </div>
                <br />
                <a href="https://files.kde.org/neon/images/developer/<?php print $isoDates->developer ?>/neon-developer-<?php print $isoDates->developer ?>.iso.torrent">Torrent file</a>
            </div>

            <article>
                <div class="text-center">
                    <a href="http://srcfiles.neon.kde.org/" target="_blank" style="color: #909c9c;">Download Source ISOs</a>
                </div>
            </article>

            <article>
                <p>
                    Install using ROSA Image Writer for </p>
                <ul>
                    <li> <a href="http://wiki.rosalab.ru/ru/images/6/62/RosaImageWriter-2.6.2-win.zip" class="internal" title="RosaImageWriter-2.6.2-win.zip">Windows</a>
                    </li>
                    <li> <a href="http://wiki.rosalab.ru/ru/images/4/45/RosaImageWriter-2.6.2-lin-i686.tar.xz" class="internal" title="RosaImageWriter-2.6.2-lin-i686.tar.xz">Linux 32-bit</a>
                    </li>
                    <li> <a href="http://wiki.rosalab.ru/ru/images/7/7f/RosaImageWriter-2.6.2-lin-x86_64.tar.xz" class="internal" title="RosaImageWriter-2.6.2-lin-x86_64.tar.xz">Linux 64-bit</a>
                    </li>
                    <li> <a href="http://wiki.rosalab.ru/ru/images/3/33/RosaImageWriter-2.6.2-osx.dmg" class="internal" title="RosaImageWriter-2.6.2-osx.dmg">Mac OS X</a>
                    </li>
                </ul>
                <p>GPG signatures signed by <a href="https://keyserver.ubuntu.com/pks/lookup?op=get&search=0xDEACEA00075E1D76">KDE
                neon ISO Signing Key</a> (0xDEACEA00075E1D76) are available alongside the ISOs for verification.</p>
            </article>

            <article>
                <h1 id="shells">Shells</h1>
                <p>
                  <a href="https://www.shells.com/pricing?special=kde&_a=kdeorg"><img src="images/shells-green.png" class="splashImage" /></a>
                  Get a powerful, secure desktop that you can take anywhere. Access your Shells on multiple devices from a smart TV to your smartphone. You can even breathe fresh life into that old computer. Make your next computer a computer in the cloud with Shells.
                </p>
                <p>
                  Transform any device into a powerful, secure desktop with Shells.  Whether you want to code on your smartphone or produce a fresh new track on your TV, with Shells you can unlock the full performance and experience of a desktop computer on any device.
                </p>
                <div class="downloadButton">
                    <a href="https://www.shells.com/pricing?special=kde&_a=kdeorg">Get your Plasma on Shells</a>
                </div>
                <p style="font-size: smaller">KDE earns a small commission when you use this link.</p>
                <p>
                  <a href="https://www.shells.com/pricing?special=kde&_a=kdeorg"><img src="images/shells-neon.png" width="600" class="splashImage" /></a><br />
                  <em>A Shells desktop running KDE neon in a web browser</em>
                </p>
            </article>

            <article>
                <h1 id="live">Docker Images</h1>
                <p>
                    <a href="https://community.kde.org/Neon/Docker"><img src="images/moby.svg" class="splashImage" width="400" /></a>
                    Docker Images are great for testing or developing on KDE software without the need to re-install your operating system or a virtual machine. We build ours daily on Docker Hub.
                    <div class="downloadButton">
                        <a href="https://community.kde.org/Neon/Docker">Docker Images Documentation</a>
                    </div>
                </p>
            </article>

            <article>
                <h1 id="snap">Snap Packages</h1>
                <p>
                    <a href="https://snapcraft.io/publisher/kde"><img src="https://snapcraft.io/static/images/badges/en/snap-store-black.svg" class="splashImage" /></a>
                    Many KDE apps now are available as Snap packages which can be installed on any Linux distro. Most of these are currently made from KDE neon packaging on the KDE neon servers.
                </p>
                <p>
                    First <a href='https://tutorials.ubuntu.com/tutorial/basic-snap-usage'>set up Snap on your system</a>, you can then install packages from the Store or through Plasma Discover.
                </p>
                <p>
                    Builds from releases go into the Candidate channel and when confirmed good into the Release channel. Builds from Git master go into Edge channel and from Git beta branches into the Stable channel.
                </p>
                <div class="downloadButton">
                    <a href="https://snapcraft.io/publisher/kde">KDE on the Snap Store</a>
                </div>
            </article>

            <article>
                <div class="text-left">
                    <p>Special Case Editions:</p>
                    <ul>
                        <li><a href="https://files.kde.org/neon/images/ko/current/">Korean Testing Edition Live/Install Image</a> pre-configured with Korean locale.</li>
                        <li><a href="https://files.kde.org/neon/images/pinebook-remix-nonfree/">Pinebook Remix Images</a> build with some non-free drivers for the ARM based Pinebook laptop.</li>
                    </ul>
                </div>
            </article>            
        </div>
    </section>
</main>

<?php
    include ('templates/footer.php');
